//const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require("path");
module.exports = {
  devtool: "inline-sourcemap",
  context: __dirname,
  entry: [__dirname + "/src/_js/index.js", __dirname + "/src/_sass/app.scss"],
  output: {
    path: __dirname + "/build/",
    publicPath: "/",
    filename: "app.js"
  },
  module: {
    rules: [
      { test: /\.(js)$/, use: "babel-loader" },
      {
        test: /\.(png|jpg|svg)$/,
        include: path.join(__dirname, "/images"),
        loader: "url-loader?limit=30000&name=images/[name].[ext]"
      },
      {
        test: /.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {
              loader: "css-loader",
              options: {
                minimize: false,
                localIdentName: "[local]"
              }
            },
            {
              loader: "sass-loader"
            }
          ]
        })
      }
    ]
  },
  plugins: [new ExtractTextPlugin("[name].css")]
};
